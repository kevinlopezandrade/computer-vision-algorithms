function [particles particles_w] = resample(particles,particles_w)

        n = size(particles,1);

        idxs = randsample(n,n,true,particles_w);
        particles = particles(idxs,:);
        particles_w = particles_w(idxs);

        particles_w = particles_w / sum(particles_w);

end
