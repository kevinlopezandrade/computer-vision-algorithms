function hist = color_histogram(xMin,yMin,xMax,yMax,frame,hist_bin)

        xMin = round(max(1,xMin));
        yMin = round(max(1,yMin));
        xMax = round(min(xMax,size(frame,2)));
        yMax = round(min(yMax,size(frame,1)));

        box = frame(yMin:yMax,xMin:xMax,:);

        r = box(:,:,1);
        g = box(:,:,2);
        b = box(:,:,3);


        [h_r,~] = imhist(r,hist_bin);
        [h_g,~] = imhist(g,hist_bin);
        [h_b,~] = imhist(b,hist_bin);


        hist = [h_r;h_g;h_b];
        hist = hist/sum(hist);

end
