% Use these variables to enable/disable different parts of the script.
%
loadImages           = true;  % also displays silhouettes
displayImages        = false;
displayVolumeCorners = false;
displayVolumeInRows  = false;
computeVisualHull    = true;
displayVolumeSlices  = false;
displayIsoSurface    = true;

%
% Adjust these variables, one at a time, to get a good visual hull.
%

% Task 9.1 silhouette threshold
silhouetteThreshold = 110;

% Task 9.2 define bounding box
%bbox = [0, 0, 0; 3, 3, 3]; % [minX minY minZ; maxX maxY maxZ];
bbox = [0.45, -0.25, -1.6; 2, 1.5, 2.9]; % [minX minY minZ; maxX maxY maxZ];
volumeX = 64;
volumeY = 64;
volumeZ = 128;
volumeThreshold = 17;

home;
numCameras = 18;

if loadImages
    % Load silhouette images and projection matrices
    for n=1:numCameras
        Ps{n} = textread(sprintf('../data/david_%02d.pa',n-1));
        Ps{n} = [eye(3,2) [1 1 1]']*Ps{n};  % add 1 for one-based indices
        ims{n} = imread(sprintf('../data/david_%02d.jpg',n-1));
        sils{n} = rgb2gray(ims{n})>silhouetteThreshold;
        
        if displayImages
                figure(1);
                subplot(1,2,1);
                imshow(sils{n});
                subplot(1,2,2);
                imshow(double(rgb2gray(ims{n}))/255.*sils{n});
                drawnow;
        end
    end
end

% Define transformation from volume to world coordinates.
T = [eye(4,3) [bbox(1,:) 1]'] * ...
    diag([(bbox(2,1)-bbox(1,1))/volumeX ...
          (bbox(2,2)-bbox(1,2))/volumeY ...
          (bbox(2,3)-bbox(1,3))/volumeZ ...
          1]);
T = [1  0 0 0; ...
     0  0 1 0; ...  % flip y and z axes for better display in matlab figure (isosurface)
     0 -1 0 0; ...
     0  0 0 1] * T;
T = T*[eye(4,3) [-[1 1 1] 1]'];  % subtract 1 for one-based indices

if displayVolumeCorners
    % Draw projection of volume corners.
    for n=1:numCameras
        figure(2);
        hold off;
        imshow(ims{n});
        hold on;
        corners = [[      0       0       0 1]' ...
                   [      0       0 volumeZ 1]' ...
                   [      0 volumeY       0 1]' ...
                   [      0 volumeY volumeZ 1]' ...
                   [volumeX       0       0 1]' ...
                   [volumeX       0 volumeZ 1]' ...
                   [volumeX volumeY       0 1]' ...
                   [volumeX volumeY volumeZ 1]'];
        pcorners = Ps{n}*T*corners;
        pcorners = pcorners./repmat(pcorners(3,:),3,1);
        plot(pcorners(1,:),pcorners(2,:),'g*');

        plot(pcorners(1,[1,2]), pcorners(2,[1,2]),'g');
        plot(pcorners(1,[3,4]), pcorners(2,[3,4]),'g');
        plot(pcorners(1,[5,6]), pcorners(2,[5,6]),'g');
        plot(pcorners(1,[7,8]), pcorners(2,[7,8]),'g');

        plot(pcorners(1,[1,3]), pcorners(2, [1,3]),'g');
        plot(pcorners(1,[2,4]), pcorners(2, [2,4]),'g');
        plot(pcorners(1,[5,7]), pcorners(2, [5,7]),'g');
        plot(pcorners(1,[6,8]), pcorners(2, [6,8]),'g');

        plot(pcorners(1,[1,5]), pcorners(2, [1,5]),'g');
        plot(pcorners(1,[2,6]), pcorners(2, [2,6]),'g');
        plot(pcorners(1,[3,7]), pcorners(2, [3,7]),'g');
        plot(pcorners(1,[4,8]), pcorners(2, [4,8]),'g');

        drawnow;
        pause(0.1);
    end
end

if displayVolumeInRows
    % Display two figures with the correspongin bounding box in each image
    i = 1;
    j = 1;
    for n=1:numCameras
        figure(mod(n,2)+1);
        %hold off;
        if mod(n,2) + 1 == 1
                subplot(3,3,i);
                i = i+1;
        else
                subplot(3,3,j);
                j = j+1;
        end
        imshow(ims{n});
        hold on;
        corners = [[      0       0       0 1]' ...
                   [      0       0 volumeZ 1]' ...
                   [      0 volumeY       0 1]' ...
                   [      0 volumeY volumeZ 1]' ...
                   [volumeX       0       0 1]' ...
                   [volumeX       0 volumeZ 1]' ...
                   [volumeX volumeY       0 1]' ...
                   [volumeX volumeY volumeZ 1]'];
        pcorners = Ps{n}*T*corners;
        pcorners = pcorners./repmat(pcorners(3,:),3,1);
        plot(pcorners(1,:),pcorners(2,:),'g*');

        plot(pcorners(1,[1,2]), pcorners(2,[1,2]),'g');
        plot(pcorners(1,[3,4]), pcorners(2,[3,4]),'g');
        plot(pcorners(1,[5,6]), pcorners(2,[5,6]),'g');
        plot(pcorners(1,[7,8]), pcorners(2,[7,8]),'g');

        plot(pcorners(1,[1,3]), pcorners(2, [1,3]),'g');
        plot(pcorners(1,[2,4]), pcorners(2, [2,4]),'g');
        plot(pcorners(1,[5,7]), pcorners(2, [5,7]),'g');
        plot(pcorners(1,[6,8]), pcorners(2, [6,8]),'g');

        plot(pcorners(1,[1,5]), pcorners(2, [1,5]),'g');
        plot(pcorners(1,[2,6]), pcorners(2, [2,6]),'g');
        plot(pcorners(1,[3,7]), pcorners(2, [3,7]),'g');
        plot(pcorners(1,[4,8]), pcorners(2, [4,8]),'g');
    end
end




if computeVisualHull
    % Define volume.
    volume = zeros(volumeX,volumeY,volumeZ);
    % Visual hull computation    
    % Task 9.3 Visual hull computation
    %   - add one to volume if projection is within silhouette region

    for n=1:numCameras
            for i=1:volumeX
                    for j=1:volumeY
                            for k=1:volumeZ
                                    imageProjection = Ps{n}*T*[i,j,k,1]';
                                    x = round(imageProjection(1)/imageProjection(3));
                                    y = round(imageProjection(2)/imageProjection(3));
                                    
                                    %Check within image and positive-negative

                                    if (x>0 && y>0)  && (x < size(sils{n}, 2) && y < size(sils{n}, 1))
                                            if sils{n}(y,x) == 1
                                                    volume(i, j, k) = volume(i, j, k) + 1;
                                            end
                                    end
                            end
                    end
            end
    end

end


if displayVolumeSlices
    figure(3);
    hold off;
    for n=1:size(volume,3)
        imagesc(volume(:,:,n));
        drawnow;
        pause(0.1);
    end
end

if displayIsoSurface
    % display result
    figure(4);
    clf;
    grid on;
    xlabel('x');
    ylabel('y');
    zlabel('z');
    hold on;
    [xMesh yMesh zMesh] = meshgrid(1:volumeY,1:volumeX,1:volumeZ);
    pt = patch(isosurface(yMesh, xMesh, zMesh, volume, volumeThreshold));
    set(pt,'FaceColor','red','EdgeColor','none');
    axis equal;
    daspect([volumeX/(bbox(2,1)-bbox(1,1)) volumeY/(bbox(2,2)-bbox(1,2)) volumeZ/(bbox(2,3)-bbox(1,3))]);
    camlight(0,0);
    camlight(180,0);
    camlight(0,90);
    camlight(0,-90);
    lighting phong;
    view(30,30);
end
