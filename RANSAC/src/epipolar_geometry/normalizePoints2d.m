% Normalization of 2d-pts
% Inputs: 
%           xs = 2d points
% Outputs:
%           nxs = normalized points
%           T = 3x3 normalization matrix
%               (s.t. nx=T*x when x is in homogenous coords)
function [nxs, T] = normalizePoints2d(xs)
        xs_centroid = mean(xs,2);
        s = sqrt(2)/mean(sqrt(sum((xs-xs_centroid).^2)));
        T = diag([s s 1]);
        T(1:2,3) = -xs_centroid(1:2,:) * s;
        nxs = T*xs;
end
