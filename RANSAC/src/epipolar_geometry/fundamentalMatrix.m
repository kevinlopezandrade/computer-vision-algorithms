% Compute the fundamental matrix using the eight point algorithm
% Input
% 	x1s, x2s 	Point correspondences
%
% Output
% 	Fh 			Fundamental matrix with the det F = 0 constraint
% 	F 			Initial fundamental matrix obtained from the eight point algorithm
%

function [Fh, F] = fundamentalMatrix(x1s, x2s)

        [nx1s, T1] = normalizePoints2d(x1s);
        [nx2s, T2] = normalizePoints2d(x2s);

        npts = size(nx1s, 2);

        A = [nx2s(1,:)'.*nx1s(1,:)'   nx2s(1,:)'.*nx1s(2,:)'  nx2s(1,:)' ...
             nx2s(2,:)'.*nx1s(1,:)'   nx2s(2,:)'.*nx1s(2,:)'  nx2s(2,:)' ...
             nx1s(1,:)'             nx1s(2,:)'            ones(npts,1) ];

        s = size(A);

        [U,D,V] = svd(A);

        F = reshape(V(:,9),3,3)';

        [U,D,V] = svd(F);

        D(3,3) = 0;

        % Denormalize
        F = T2'*F*T1;
        Fh = T2' * (U * D * V') * T1;

end
