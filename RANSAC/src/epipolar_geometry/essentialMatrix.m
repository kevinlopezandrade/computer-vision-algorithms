% Compute the essential matrix using the eight point algorithm
% Input
% 	x1s, x2s 	Point correspondences 3xn matrices
%
% Output
% 	Eh 			Essential matrix with the det F = 0 constraint and the constraint that the first two singular values are equal
% 	E 			Initial essential matrix obtained from the eight point algorithm
%

function [Eh, E] = essentialMatrix(x1s, x2s)
        [nx1s, T1] = normalizePoints2d(x1s);
        [nx2s, T2] = normalizePoints2d(x2s);

        npts = size(nx1s, 2);

        A = [nx2s(1,:)'.*nx1s(1,:)'   nx2s(1,:)'.*nx1s(2,:)'  nx2s(1,:)' ...
             nx2s(2,:)'.*nx1s(1,:)'   nx2s(2,:)'.*nx1s(2,:)'  nx2s(2,:)' ...
             nx1s(1,:)'             nx1s(2,:)'            ones(npts,1) ];

        s = size(A);

        [U,D,V] = svd(A);

        E = reshape(V(:,9),3,3)';

        [U,D,V] = svd(E);

        s = (D(1,1) + D(2,2))/2;
        D(1,1) = s;
        D(2,2) = s;
        D(3,3) = 0;


        % Denormalize
        E = T2'*E*T1;
        Eh = T2' * (U * D * V') * T1;

end
