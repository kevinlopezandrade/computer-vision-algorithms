function [k, b] = ransacLine(data, iter, threshold)
        % https://en.wikipedia.org/wiki/Random_sample_consensus 
        % data: a 2xn dataset with #n data points
        % iter: the number of iterations
        % threshold: the threshold of the distances between points and the fitting line

        num_pts = size(data,2); % Total number of points

        best_inliers = 0;       % Best fitting line with largest number of inliers

        k=0; 
        b=0;                % parameters for best fitting line
        inlierRatio = 0.1;

        for i=1:iter
            % Randomly select 2 points and fit line to these
            % Tip: Matlab command randperm is useful here 

            idx = randperm(num_pts, 2);
            sample = data(:, idx);


            % Model is y = k*x + b
            
            % Compute the distances between all points with the fitting line    

            kLine = sample(:, 2) - sample(:, 1);
            kLineNorm = kLine/norm(kLine);
            normVector = [-kLineNorm(2), kLineNorm(1)];
            distance = normVector*(data - repmat(sample(:, 1), 1, num_pts));
                
            % Compute the inliers with distances smaller than the threshold

            inlierIdx = find(abs(distance)<=threshold);
            inlierNum = length(inlierIdx);
                
            % Update the number of inliers and fitting model if better model is found

            if inlierNum>=round(inlierRatio*num_pts) && inlierNum>best_inliers
                    best_inliers = inlierNum;
                    kAux = (sample(2,2)-sample(2,1))/(sample(1,2)-sample(1,1));
                    bAux = sample(2,1)-k*sample(1,1);

                    k = kAux;
                    b = bAux;
                    
            end

            
        end
end
