function [xyn, XYZn, T, U] = normalization(xy, XYZ)


xy(1, :) = rdivide(xy(1, :), xy(3, :));
xy(2, :) = rdivide(xy(2, :), xy(3, :));
xy(3, :) = rdivide(xy(3, :), xy(3, :));



XYZ(1, :) = rdivide(XYZ(1, :), XYZ(4, :));
XYZ(2, :) = rdivide(XYZ(2, :), XYZ(4, :));
XYZ(3, :) = rdivide(XYZ(3, :), XYZ(4, :));
XYZ(4, :) = rdivide(XYZ(4, :), XYZ(4, :));

%data normalization
%first compute centroid
% ignore the last entry of the homogenous vectors
xy_centroid = mean(xy(1:2, :)')';
XYZ_centroid = mean(XYZ(1:3, :)')';

% Then, transform the input points so that the centroid of the points is at the origin

xyt = [xy(1:2, :)-xy_centroid; xy(3, :)];

XYZt = [XYZ(1:3, :)-XYZ_centroid; XYZ(4, :)];



%then, compute scale
xy_meand = sqrt(xyt(1,:).^2 + xyt(2,:).^2);   
xy_meand = mean(xy_meand(:));
Tscale = sqrt(2)/xy_meand;


XYZ_meand = sqrt(XYZt(1,:).^2 + XYZt(2,:).^2 + XYZt(3,:).^2);
XYZ_meand = mean(XYZ_meand(:));
Uscale = sqrt(3)/XYZ_meand;

%create T and U transformation matrices

T = [Tscale   0   -Tscale*xy_centroid(1)
     0     Tscale -Tscale*xy_centroid(2)
     0       0      1      ];

U = [Uscale  0     0      -Uscale*XYZ_centroid(1)
     0     Uscale  0      -Uscale*XYZ_centroid(2)
     0       0   Uscale   -Uscale*XYZ_centroid(3)
     0       0     0        1    ];

%and normalize the points according to the transformations
xyn = T*xy;
XYZn = U*XYZ;

end
