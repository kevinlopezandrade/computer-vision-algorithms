function [ K, R, C ] = decompose(P)
%decompose P into K, R and t
p1 = P(:,1);
p2 = P(:,2);
p3 = P(:,3);    

M = [p1 p2 p3];
C = null(P,'r');

[R1 K1] = qr(inv(M));
K = inv(K1);
K=K./K(3,3);
R = inv(R1);    

end
