function [map peak] = meanshiftSeg(img)
        img = double(img);
        L = size(img,1)*size(img,2)
        X = reshape(img, L, 3); % Lx3 Matrix

        map = zeros(1,L);
        peak = [];

        r = 20;

        for i = 1:L

                fprintf('Finding peak for pixel number %d \n', i);

                mode = find_peak(X, X(i, :), r)';

                if i == 1
                        peak = [peak mode];
                        map(i) = size(peaks, 2);
                else
                        sqDistToAll = sum((repmat(mode,1,size(peak,2)) - peak).^2); % Distance to the other peaks

                        mergeWith =  find(sqDistToAll < r/2, 1);

                        if isempty(mergeWith)
                                peak = [peak mode];
                                map(i) = size(peak, 2);
                        else
                                map(i) = mergeWith;
                        end
                end
        end
        
        fprintf('%d clusters detected \n', size(peaks, 2));
        peak = peak'; % Peak = Vals and Vals is matrix of K values of dimension D
        map = reshape(map, size(img, 1), size(img, 2));
end
