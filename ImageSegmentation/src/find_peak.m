function peak = find_peak(X, xl, r)
        Xt = X';
        [numDim, numPts] = size(Xt);
        e = .01;

        myMean = xl';

        while true
                sqDistToAll = sum((repmat(myMean,1,numPts) - Xt).^2);
                inInds      = find(sqDistToAll < r);
                myOldMean   = myMean;    
                myMean      = mean(Xt(:,inInds),2);
                shift       = norm(myMean - myOldMean);

                if shift < e
                        peak = myMean;
                        %fprintf('I converged \n');
                        break;
                end
        end

        peak = peak';
end
