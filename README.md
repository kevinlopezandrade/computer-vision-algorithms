# Computer Vision Algorithms

This repo contains matlab implementations of
classic computer vision algorithms. Those implementations
were part of the Computer Vision lecture held a ETH.

In each folder there is a short report explaining the work done.
