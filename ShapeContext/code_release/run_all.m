function runall()

        k = [1, 3, 8];

        for i = k
                fprintf('K-NN with K=%d\n', i);
                for t = 1:3
                        [accuracy, hits, nobjects] = shape_classification(i, true);
                        fprintf('Test %d | Accuracy: %d | True positives: %d/%d\n', t, accuracy, hits, nobjects);
                end
        end

end
