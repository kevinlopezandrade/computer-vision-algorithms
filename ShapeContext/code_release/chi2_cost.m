function C = chi2_cost(s1, s2)
        C = zeros(size(s1,3));
        for i = 1:size(s1,3)
                for j = 1:size(s2,3)
                        C(i,j) = 0.5 * nansum(nansum(((s1(:,:,i) - s2(:,:,j)).^2)./ ((s1(:,:,i) + s2(:,:,j)))));
                end
        end
end
