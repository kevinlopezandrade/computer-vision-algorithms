function disp = stereoDisparity(img1, img2, dispRange)
        % dispRange: range of possible disparity values
        % --> not all values need to be checked
        % Algorithm of the slides

        img1=im2double(img1);
        img2=im2double(img2);
        aux = 0;

        disp = zeros(size(img1));

        for d = dispRange

                img1_shifted = shiftImage(img1, d);
                SSD = (img2 - img1_shifted).^2;
                box_filter = fspecial('average', 25);

                Idiff = conv2(SSD, box_filter, 'same');

                if aux == 0
                        bestDiff = Idiff;
                        disp = disp + d;
                else
                        mask = Idiff < bestDiff; % matrix of zeros and d
                        mask_reverse = Idiff >= bestDiff;
                        disp = disp.*mask_reverse + d.*mask;
                        bestDiff = bestDiff.*mask_reverse + Idiff.*mask;
                end
                
                aux = aux + 1;
        end
end
