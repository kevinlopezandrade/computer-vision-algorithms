function diffs = diffsGC(img1, img2, dispRange)
        
        img1=im2double(img1);
        img2=im2double(img2);

        % get data costs for graph cut

        [r c]=size(img1);

        diffs=zeros(r,c,length(dispRange));

        for i=1:length(dispRange) 
            img1Shifted = shiftImage( img1, dispRange(i) );
            SSD=(img2-img1Shifted).^2;
            
            H = fspecial('average',5);
            SSD=conv2(SSD,H,'same');
            
            diffs(:,:,i)= SSD;

end
