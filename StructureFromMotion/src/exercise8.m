% =========================================================================
% Exercise 8
% =========================================================================

% Initialize VLFeat (http://www.vlfeat.org/)

%K Matrix for house images (approx.)
K = [  670.0000     0     393.000
         0       670.0000 275.000
         0          0        1];

%Load images
imgName1 = '../data/house.000.pgm';
imgName2 = '../data/house.004.pgm';

img1 = single(imread(imgName1));
img2 = single(imread(imgName2));

%extract SIFT features and match
[fa, da] = vl_sift(img1);
[fb, db] = vl_sift(img2);

%don't take features at the top of the image - only background
filter = fa(2,:) > 100;
fa = fa(:,find(filter));
da = da(:,find(filter));

[matches, scores] = vl_ubcmatch(da, db);

%showFeatureMatches(img1, fa(1:2, matches(1,:)), img2, fb(1:2, matches(2,:)), 20);

%% Compute essential matrix and projection matrices and triangulate matched points

%use 8-point ransac or 5-point ransac - compute (you can also optimize it to get best possible results)
%and decompose the essential matrix and create the projection matrices

x1 = fa(1:2, matches(1, :));
x2 = fb(1:2, matches(2, :));

x1h = makehomogeneous(x1);
x2h = makehomogeneous(x2);

[F, inliers] = ransacfitfundmatrix(x1, x2, 0.008);

% Show the inliers
%showFeatureMatches(img1, x1(:, inliers), img2, x2(:, inliers), 20);

% Show the outliers
outliers = setdiff(1:size(matches, 2), inliers);
%showFeatureMatches(img1, x1(:, outliers), img2, x2(:, outliers), 22);



% show epipolar lines
%figure(2), imshow(img1, []); hold on;
%for k = 1:10:size(inliers, 2)
%    plot(x1(1, inliers(k)), x1(2, inliers(k)), '*b');
%    drawEpipolarLines(F'*x2h(:, inliers(k)), img1);    
%end
%
%figure(3), imshow(img2, []); hold on;
%for k = 1:10:size(inliers, 2)
%    plot(x2(1, inliers(k)), x2(2, inliers(k)), '*b');
%    drawEpipolarLines(F*x1h(:, inliers(k)), img2);
%end


% Compute the essential matrix
E = K' * F * K;

x1_calibrated = K  \ x1h(:, inliers);
x2_calibrated = K \ x2h(:, inliers);


Ps{1} = eye(4);
Ps{2} = decomposeE(E, x1_calibrated, x2_calibrated);

%triangulate the inlier matches with the computed projection matrix

[Xs, err] = linearTriangulation(Ps{1}, x1_calibrated, Ps{2}, x2_calibrated);


%% Add an addtional view of the scene 

imgName3 = '../data/house.002.pgm';
img3 = single(imread(imgName3));
[fc, dc] = vl_sift(img3);

%match against the features from image 1 that where triangulated

fa_inliers = fa(:, matches(1, inliers));
da_inliers = da(:, matches(1, inliers));

[matches2, scores2] = vl_ubcmatch(da_inliers, dc);

x1_img3 = fa_inliers(1:2, matches2(1, :));
x3 = fc(1:2, matches2(2, :));

x1h_img3 = makehomogeneous(x1_img3);
x3h = makehomogeneous(x3);

%run 6-point ransac

x3_calibrated = K \ x3h;

[Ps{3}, inliers2] = ransacfitprojmatrix(x3_calibrated, Xs(:, matches2(1, :)), 0.008);

% Show the inliers
%showFeatureMatches(img1, x1_img3(:, inliers2), img3, x3(:, inliers2), 20);

% Show the outliers
outliers2 = setdiff(1:size(matches2, 2), inliers2);
%showFeatureMatches(img1, x1_img3(:, outliers2), img3, x3(:, outliers2), 22);

if (det(Ps{3}(1:3,1:3)) < 0 )
    Ps{3}(1:3,1:3) = -Ps{3}(1:3,1:3);
    Ps{3}(1:3, 4) = -Ps{3}(1:3, 4);
end

%triangulate the inlier matches with the computed projection matrix


x1_img3_calibrated = K \ x1h_img3(:, inliers2);
x3_calibrated_inliers = x3_calibrated(:, inliers2);

[Xs2, err2] = linearTriangulation(Ps{1}, x1_img3_calibrated, Ps{3}, x3_calibrated_inliers);


%% Add more views 1...

imgName4 = '../data/house.001.pgm';
img4 = single(imread(imgName4));
[fd, dd] = vl_sift(img4);

%match against the features from image 1 that where triangulated

fa_inliers = fa(:, matches(1, inliers));
da_inliers = da(:, matches(1, inliers));

[matches3, scores3] = vl_ubcmatch(da_inliers, dd);

x1_img4 = fa_inliers(1:2, matches3(1, :));
x4 = fd(1:2, matches3(2, :));

x1h_img4 = makehomogeneous(x1_img4);
x4h = makehomogeneous(x4);

%run 6-point ransac

x4_calibrated = K \ x4h;

[Ps{4}, inliers3] = ransacfitprojmatrix(x4_calibrated, Xs(:, matches3(1, :)), 0.008);

% Show the inliers
%showFeatureMatches(img1, x1_img4(:, inliers3), img4, x4(:, inliers3), 20);

% Show the outliers
outliers3 = setdiff(1:size(matches3, 2), inliers3);
%showFeatureMatches(img1, x1_img4(:, outliers3), img4, x4(:, outliers3), 22);

if (det(Ps{3}(1:3,1:3)) < 0 )
    Ps{3}(1:3,1:3) = -Ps{3}(1:3,1:3);
    Ps{3}(1:3, 4) = -Ps{3}(1:3, 4);
end

%triangulate the inlier matches with the computed projection matrix


x1_img4_calibrated = K \ x1h_img4(:, inliers3);
x4_calibrated_inliers = x4_calibrated(:, inliers3);

[Xs3, err3] = linearTriangulation(Ps{1}, x1_img4_calibrated, Ps{4}, x4_calibrated_inliers);

%% Add more views 2...

imgName5 = '../data/house.003.pgm';
img5 = single(imread(imgName5));
[fe, de] = vl_sift(img5);

%match against the features from image 1 that where triangulated

fa_inliers = fa(:, matches(1, inliers));
da_inliers = da(:, matches(1, inliers));

[matches4, scores4] = vl_ubcmatch(da_inliers, de);

x1_img5 = fa_inliers(1:2, matches4(1, :));
x5 = fe(1:2, matches4(2, :));

x1h_img5 = makehomogeneous(x1_img5);
x5h = makehomogeneous(x5);

%run 6-point ransac

x5_calibrated = K \ x5h;

[Ps{5}, inliers4] = ransacfitprojmatrix(x5_calibrated, Xs(:, matches4(1, :)), 0.008);

% Show the inliers
%showFeatureMatches(img1, x1_img5(:, inliers4), img5, x5(:, inliers4), 20);

% Show the outliers
outliers4 = setdiff(1:size(matches4, 2), inliers4);
%showFeatureMatches(img1, x1_img5(:, outliers4), img5, x5(:, outliers4), 22);

if (det(Ps{3}(1:3,1:3)) < 0 )
    Ps{3}(1:3,1:3) = -Ps{3}(1:3,1:3);
    Ps{3}(1:3, 4) = -Ps{3}(1:3, 4);
end

%triangulate the inlier matches with the computed projection matrix


x1_img5_calibrated = K \ x1h_img5(:, inliers4);
x5_calibrated_inliers = x5_calibrated(:, inliers4);

[Xs4, err4] = linearTriangulation(Ps{1}, x1_img5_calibrated, Ps{5}, x5_calibrated_inliers);

%% Plot stuff

fig = 10;
figure(fig);

%use plot3 to plot the triangulated 3D points
plot3(Xs(1,:), Xs(2,:), Xs(3,:), 'r.');
hold on;
plot3(Xs2(1,:), Xs2(2,:), Xs2(3,:), 'g.')
hold on;
plot3(Xs3(1,:), Xs3(2,:), Xs3(3,:), 'b.')
hold on;
plot3(Xs4(1,:), Xs4(2,:), Xs4(3,:), 'y.')
hold on;
%draw cameras
drawCameras(Ps, fig);
