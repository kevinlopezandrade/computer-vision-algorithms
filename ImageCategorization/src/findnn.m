function [Idx Dist] = findnn( D1, D2 )
        % input:
        %   D1  : NxD matrix containing N feature vectors of dim. D
        %   D2  : MxD matrix containing M feature vectors of dim. D
        % output:
        %   Idx : N-dim. vector containing for each feature vector in D1
        %         the index of the closest feature vector in D2.
        %   Dist: N-dim. vector containing for each feature vector in D1
        %         the distance to the closest feature vector in D2.

        N = size(D1,1);
        M = size(D2,1);
        Idx  = zeros(N,1);
        Dist = zeros(N,1);

        % Find for each feature vector in D1 the nearest neighbor in D2
        ...
        ...

        
        i = 1;
        for vectorD1 = D1'
                j = 1;
                minDistance = Inf;
                minIndex = 1;

                for vectorD2 = D2'
                        euclideanDistance = norm(vectorD1 - vectorD2);

                        if euclideanDistance < minDistance
                                minDistance = euclideanDistance;
                                minIndex = j;
                        end

                        j = j+1;
                end

                Idx(i) = minIndex;
                Dist(i) = minDistance;

                i = i+1;
        end

end

