function [mu sigma] = computeMeanStd(vBoW)

        meanEstimator = mean(vBoW, 1);
        mu = meanEstimator;

        sigmaEstimator = std(vBoW);
        sigma = sigmaEstimator;

end
