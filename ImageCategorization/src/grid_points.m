function vPoints = grid_points(img,nPointsX,nPointsY,border)
        [height, width] = size(img);

        x = round(linspace(border+1, width-border, nPointsX));
        y = round(linspace(border+1, height-border, nPointsY));

        vPoints = [];

        for i = 1:nPointsX
                for j = 1:nPointsY
                        gridPoints = [x(i), y(j)]';
                        vPoints = [vPoints gridPoints];
                end
        end

        vPoints = vPoints';
    
        %{
        imshow(img);
        hold on;
        plot(vPoints(:, 1), vPoints(:, 2), 'b*');
        hold off;
        %}
end
