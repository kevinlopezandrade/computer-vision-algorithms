function vCenters = kmeans(vFeatures,k,numiter)

        nPoints  = size(vFeatures,1);
        nDims    = size(vFeatures,2);
        vCenters = zeros(k,nDims);

        % Initialize each cluster center to a different random point.
        ...
        ...

        randIndex = randperm(nPoints, k);
        vCenters = vFeatures(randIndex, :);

        % Repeat for numiter iterations
        for i=1:numiter
            disp(strcat(num2str(i),'/',num2str(numiter),' iteration started.'));
                % Assign each point to the closest cluster
                ...
                ...

                [Idx, Dist] = findnn(vFeatures, vCenters);

                % Shift each cluster center to the mean of its assigned points
                ...
                ...

                for j=1:k
                        clusterRange = find(Idx == j)';
                        clusterK = vFeatures(clusterRange, :);
                        vCenters(j, :) = mean(clusterK,1);
                end


            disp(strcat(num2str(i),'/',num2str(numiter),' iterations completed.'));
        end

end
