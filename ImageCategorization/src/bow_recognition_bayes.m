function label = bow_recognition_bayes( histogram, vBoWPos, vBoWNeg)

        [muPos sigmaPos] = computeMeanStd(vBoWPos);
        [muNeg sigmaNeg] = computeMeanStd(vBoWNeg);

        % Calculating the probability of appearance each word in observed histogram
        % according to normal distribution in each of the positive and negative bag of words

        priorPositive = 0.5;
        priorNegative = 0.5;

        positiveLikelihood = 0;
        negativeLikelihood = 0;


        positiveLikelihood = exp(nansum(log(normpdf(histogram,muPos,sigmaPos))));
        negativeLikelihood = exp(nansum(log(normpdf(histogram,muNeg,sigmaNeg))));

        posteriorPositive = positiveLikelihood * priorPositive; 
        posteriorNegative = negativeLikelihood * priorNegative;


        if posteriorPositive > posteriorNegative
                label = 1;
        else
                label = 0;
        end

end
