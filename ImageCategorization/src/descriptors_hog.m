function [descriptors, patches] = descriptors_hog(img,vPoints,cellWidth,cellHeight)

    nBins = 8;
    w = cellWidth; % set cell dimensions
    h = cellHeight;   

    test = [];

    descriptors = zeros(0,nBins*4*4); % one histogram for each of the 16 cells
    patches = zeros(0,4*w*4*h); % image patches stored in rows    
    
    [grad_x,grad_y]=gradient(img);    

    
    for i = [1:size(vPoints,1)] % for all local feature points

            xRangeCell = [vPoints(i,1)-2*w:vPoints(i,1)-1, vPoints(i,1)+1:vPoints(i,1)+ 2*w];
            yRangeCell = [vPoints(i,2)-2*h:vPoints(i,2)-1, vPoints(i,2)+1:vPoints(i,2)+ 2*h];


            test = img(yRangeCell, xRangeCell);
            patch = img(yRangeCell, xRangeCell);

            magnitudePatch = sqrt(grad_x(yRangeCell, xRangeCell).^2 + grad_y(yRangeCell, xRangeCell).^2);
            directionPatch = atand(grad_y(yRangeCell, xRangeCell)./grad_x(yRangeCell, xRangeCell));

            cells = mat2cell(directionPatch, [4 4 4 4], [4 4 4 4]);

            histogramForGridPoint = zeros(0, 8);

            for k = 1:length(cells)*length(cells)
                    cell = cells{k};
                    [hg, ~] = histcounts(reshape(cell, [1 16]),8);
                    histogramForGridPoint(k, :) = hg;
            end

            histogramForGridPoint = reshape(histogramForGridPoint', [1 128]);

            descriptors(i, :) = histogramForGridPoint;
            patches(i, :) = reshape(patch, [1 256]);

    end % for all local feature points

    
end
