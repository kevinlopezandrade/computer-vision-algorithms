% extract harris corner
%
% Input:
%   img           - n x m gray scale image
%   thresh        - scalar value to threshold corner strength
%   
% Output:
%   corners       - 2 x k matrix storing the keypoint coordinates
%   H             - n x m gray scale image storing the corner strength
function [corners, H] = extractHarrisCorner(img, thresh)

[Ix, Iy] = gradient(img);

max_size = 3;

% Convolution with the gaussian
sigma = 2;

g = fspecial('gaussian',5, sigma);

Ix2 = conv2(Ix.^2, g, 'same'); 
Iy2 = conv2(Iy.^2, g, 'same');
Ixy = conv2(Ix.*Iy, g, 'same');


H = (Ix2.*Iy2 - Ixy.^2)./(Ix2 + Iy2 + eps); 

non_max = ordfilt2(H,max_size^2,ones(max_size)); 

H = (H==non_max)&(H>thresh);
%H = (H>thresh);

[rows,cols] = find(H);

corners = [rows';cols'];

end
