import cv2
import numpy as np
from matplotlib import pyplot as plt

filename = 'I1.jpg'
img = cv2.imread(filename)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# Threshold for an optimal value, it may vary depending on the image.
img[dst>0.01*dst.max()]=[0,0,255]

small = cv2.resize(img, (0,0), fx=0.2, fy=0.2) 
cv2.imshow('dst', small)
cv2.waitKey(0)
cv2.destroyAllWindows()
# plot.subplot()
# plt.imshow(dst)
# plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
# plt.show()
