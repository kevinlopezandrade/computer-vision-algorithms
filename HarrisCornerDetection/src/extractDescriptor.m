% extract descriptor
%
% Input:
%   keyPoints     - detected keypoints in a 2 x n matrix holding the key
%                   point coordinates
%   img           - the gray scale image
%   
% Output:
%   descr         - w x n matrix, stores for each keypoint a
%                   descriptor. m is the size of the image patch,
%                   represented as vector

function descr = extractDescriptor(corners, img)  

n = size(corners,2);
patch_size = 9;


descr = zeros(patch_size^2,n);

shift = (patch_size-1)/2;

img_pad = padarray(img,[shift,shift]);

for i = 1:n
    patch = img_pad(corners(1,i):corners(1,i)+2*shift, corners(2,i):corners(2,i)+2*shift);
    descr(:,i) = patch(:);
end

end
